import 'jspsych';

import get_hello from './parts/hello';
import get_instructions from "./parts/instructions";

jsPsych.init({
    timeline: [
        get_hello(),
        get_instructions(),
    ]
});