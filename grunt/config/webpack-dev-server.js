const webpack_dev = require('../../webpack.dev');

module.exports = {
    options: {
        webpack: webpack_dev,
        port: 8080,
        open: true,
    },
    start: { },
};
