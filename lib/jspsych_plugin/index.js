import 'jspsych';

class jsPsychPlugin {
    info = {
        name: ''
    }

    constructor() {
        this.jsPsych = window.jsPsych;
    }

    trial(display_element, trial) {
        throw new Error('You must overwrite the trial method.')
    }

    finish_trial(data) {
        this.jsPsych.finishTrial(data)
    }
}

export { jsPsychPlugin };
